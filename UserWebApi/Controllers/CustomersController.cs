﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UserWebApi.ViewModels;

namespace UserWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private ICustomerRepository _repo;

        public CustomersController(ICustomerRepository repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public async Task<IEnumerable<Customer>> Get(CancellationToken token)
        {
            return await _repo.SelectCustomersAsync(token);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult<Customer>> Get(int id, CancellationToken token)
        {
            var result = await _repo.GetCustomerAsync(id, token);
            if (result == null)
                return NotFound();
            return result;
        }

        [HttpPost]
        public async Task<ActionResult<CustomerViewModel>> Post(CustomerViewModel customer, CancellationToken token)
        {
            var result = await _repo.GetCustomerAsync(customer.Id, token);
            if (result != null)
                return Conflict($"Customer with id {customer.Id} aleready exists");
            var entity = ToEntity(customer);
            await _repo.AddCustomerAsync(ToEntity(customer), token);
            return ToViewModel(entity);
        }

        private Customer ToEntity(CustomerViewModel vm) => new Customer
        {
            Id = vm.Id,
            FullName = vm.FullName,
            Email = vm.Email,
            Phone = vm.Phone
        };

        private CustomerViewModel ToViewModel(Customer model) => new CustomerViewModel
        {
            Id = model.Id,
            FullName = model.FullName,
            Email = model.Email,
            Phone = model.Phone
        };
    }
}
