using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.IO;

namespace UserWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.Decorate<ICustomerRepository, CustomerRepositoryWithCache>();
            services.AddMemoryCache();
            services.AddControllers();
            services.AddDbContext<CustomerDbContext>((services, options) =>
            {
                var hostEnv = services.GetRequiredService<IHostEnvironment>();
                options.UseSqlite($"Data Source={Path.GetFullPath($"{hostEnv.ContentRootPath}\\..\\Otus.Teaching.Concurrency.Import.Loader\\bin\\Debug\\netcoreapp3.0\\customers.db")}");
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
