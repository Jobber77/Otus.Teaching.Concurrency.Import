﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        public static void Main(string[] args) => CreateHostBuilder(args).Build().Run();

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            new HostBuilder()
                .ConfigureAppConfiguration((context, config) =>
                {
                    config.AddJsonFile("appsettings.json", optional: false);
                })
                .ConfigureServices((context, services) =>
                {
                    services.Configure<DataGeneratorSettings>(context.Configuration.GetSection("DataGeneratorSettings"));
                    services.AddTransient<IDataGenerator, XmlDataGenerator>((provider) =>
                    {
                        var options = provider.GetRequiredService<IOptionsSnapshot<DataGeneratorSettings>>().Value;
                        return new XmlDataGenerator(options.GeneratedDataFileName, options.RecordsAmount);
                    });
                    services.AddTransient<IDataParser, XmlDataParser>();
                    services.AddTransient<IDataLoader<Customer>, CustomerDataLoaderAsync>();
                    services.AddTransient<ICustomerRepository, CustomerRepository>();
                    services.AddHostedService<LoaderHostedService>();
                    services.AddDbContext<CustomerDbContext>((services, options) =>
                    {
                        var hostEnv = services.GetRequiredService<IHostEnvironment>();
                        options.UseSqlite($"Data Source={hostEnv.ContentRootPath}customers.db");
                    });
                });
    }
}