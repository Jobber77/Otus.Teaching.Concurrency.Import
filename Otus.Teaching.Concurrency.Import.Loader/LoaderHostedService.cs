﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class LoaderHostedService : IHostedService
    {
        private readonly IDataGenerator _generator;
        private readonly IDataParser _parser;
        private readonly IDataLoader<Customer> _dataLoader;

        private DataGeneratorSettings _options;

        public LoaderHostedService(IOptionsSnapshot<DataGeneratorSettings> options, IDataGenerator generator, IDataParser parser, IDataLoader<Customer> dataLoader)
        {
            _options = options.Value;
            _generator = generator;
            _parser = parser;
            _dataLoader = dataLoader;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            var path = await GenerateCustomersDataFile(_options);
            Console.WriteLine($"Data file generated {path}...");
            Console.WriteLine($"Start loading at {DateTime.Now}...");
            var text = File.ReadAllText(path);
            var data = _parser.Parse<CustomersList, Customer>(text);
            Console.WriteLine($"Start saving to database...");
            await _dataLoader.LoadDataAsync(data);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private async Task<string> GenerateCustomersDataFile(DataGeneratorSettings settings)
        {
            var executableFullPath = Path.Combine(settings.SeparateProcessPath, settings.SeparateProcessExecutable);
            if (settings.UseSeparateProcess && File.Exists(executableFullPath))
            {
                Process.Start(new ProcessStartInfo(executableFullPath, $"{settings.GeneratedDataFileName} {settings.RecordsAmount}"));
                return Path.Combine(settings.SeparateProcessPath, settings.FullDataFileName);
            }

            return await _generator.GenerateAsync(CancellationToken.None);
        }
    }
}
