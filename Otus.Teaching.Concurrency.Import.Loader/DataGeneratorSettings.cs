﻿namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class DataGeneratorSettings
    {
        public bool UseSeparateProcess { get; set; }

        public string SeparateProcessPath { get; set; }

        public string SeparateProcessExecutable { get; set; }

        public int RecordsAmount { get; set; }

        public string GeneratedDataFileName { get; set; }

        public string GeneratedDataFileExtension { get; set; }

        public string FullDataFileName => $"{GeneratedDataFileName}{GeneratedDataFileExtension}";
    }
}
