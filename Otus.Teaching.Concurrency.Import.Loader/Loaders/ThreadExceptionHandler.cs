﻿using System;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public delegate void ThreadExceptionHandler(ProcessCustomerArgs args, Exception exception);
}
