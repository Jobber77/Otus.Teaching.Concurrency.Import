﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class ProcessCustomerArgs
    {
        public int Attempt { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
    }
}
