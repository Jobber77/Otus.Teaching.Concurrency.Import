﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    class CustomerDataLoaderAsync : IDataLoader<Customer>
    {
        private IServiceScopeFactory _scopeFactory;
        private const int MaxAttempts = 3;

        public CustomerDataLoaderAsync(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public void LoadData(IEnumerable<Customer> data)
        {
            throw new NotImplementedException();
        }

        public async Task LoadDataAsync(IEnumerable<Customer> data)
        {
            var middleOfArray = data.Count() / 2;
            var task1 = SaveToDatabase(data.Where(customer => customer.Id <= middleOfArray).ToArray(), MaxAttempts);
            var task2 = SaveToDatabase(data.Where(customer => customer.Id > middleOfArray).ToArray(), MaxAttempts);
            await Task.WhenAll(task1, task2);
        }

        private async Task SaveToDatabase(IEnumerable<Customer> customers, int attempts)
        {
            try
            {
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} started at {DateTime.Now}");
                await SaveEachRecord(customers);
                Console.WriteLine($"{customers.Count()} records saved.");
                Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} finished at {DateTime.Now}");
                return;
            }
            catch(Exception ex) when (attempts-- >= 0) 
            {
                Console.WriteLine($"An exception occured. This happend {MaxAttempts - attempts} times. Exception: {ex.Message}; {ex.InnerException?.Message}. " +
                    $"{Environment.NewLine} Retrying execution...");
                if (attempts == 0)
                {
                    Console.WriteLine($"An exception occured {MaxAttempts} or more times. Aborting task.");
                    return;
                }
            }
            await SaveToDatabase(customers, attempts);
        }

        private async Task SaveEachRecord(IEnumerable<Customer> customers)
        {
            using var scope = _scopeFactory.CreateScope();
            var repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();
            foreach (var customer in customers)
                await repo.AddCustomerAsync(customer, CancellationToken.None);
        }
    }
}
