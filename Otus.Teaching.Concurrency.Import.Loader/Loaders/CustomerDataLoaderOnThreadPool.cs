﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class CustomerDataLoaderOnThreadPool : IDataLoader<Customer>
    {
        public event ThreadExceptionHandler OnThreadException;
        private const int MaxAttempts = 3;
        private readonly IServiceScopeFactory _scopeFactory;

        public CustomerDataLoaderOnThreadPool(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            OnThreadException += (args, exception) =>
            {
                Console.WriteLine($"An exception occured. This happend {args.Attempt} times. Exception: {exception.Message}; {exception.InnerException?.Message}. " +
                    $"{Environment.NewLine} Retrying execution...");
                if (args.Attempt >= MaxAttempts)
                {
                    Console.WriteLine($"An exception occured {MaxAttempts} or more times. Aborting task.");
                    return;
                }
                LoadOnThreadPoolThread(args);
            };
        }

        public void LoadData(IEnumerable<Customer> data)
        {
            var middleOfArray = data.Count() / 2;
            LoadOnThreadPoolThread(new ProcessCustomerArgs { Customers = data.Where(customer => customer.Id <= middleOfArray).ToArray(), Attempt = 0 });
            LoadOnThreadPoolThread(new ProcessCustomerArgs { Customers = data.Where(customer => customer.Id > middleOfArray).ToArray(), Attempt = 0 });
        }

        private void LoadOnThreadPoolThread(ProcessCustomerArgs data) =>
            ThreadPool.QueueUserWorkItem(new WaitCallback(SaveToDatabase), data);

        private void SaveToDatabase(object customers)
        {
            if (!(customers is ProcessCustomerArgs args))
                return;
            try
            {
                SaveEachRecord(args.Customers);
                Console.WriteLine($"{args.Customers.Count()} records saved.");
                Console.WriteLine($"Thread finished at {DateTime.Now}");
            }
            catch (Exception ex)
            {
                args.Attempt++;
                OnThreadException?.Invoke(args, ex);
            }
        }

        private void SaveEachRecord(IEnumerable<Customer> customers)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var repo = scope.ServiceProvider.GetRequiredService<ICustomerRepository>();
                foreach (var customer in customers)
                    repo.AddCustomer(customer);
            }
        }

        public Task LoadDataAsync(IEnumerable<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}
