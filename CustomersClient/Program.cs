﻿using AutoFixture;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.IO;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace CustomersClient
{
    class Program
    {
        private static IConfigurationRoot _config;

        static async Task Main(string mode = "get")
        {
            _config = GetConfig();
            var task = mode.ToLower() switch
            {
                "post" => PostCustomer(),
                _ => GetCustomer()
            };
            await task;
            Console.ReadLine();
        }

        private static async Task GetCustomer()
        {
            Console.WriteLine("Applicaton Mode - GET. Enter Customer Id to get it.");
            var idString = Console.ReadLine();
            var isParsed = int.TryParse(idString, out int id);
            if (!isParsed)
                return;
            using var httpClient = new HttpClient();
            var result = await httpClient.GetAsync($"{_config["ApiEndpoint"]}{id}");
            var customerString = await result.Content.ReadAsStringAsync();
            Console.WriteLine($"Fetched Customer: {customerString}");
        }

        private static async Task PostCustomer()
        {
            Console.WriteLine("Applicaton Mode - POST. Generating and posting random customer.");
            using var httpClient = new HttpClient();
            var customer = new Fixture().Create<Customer>();
            var content = new StringContent(JsonSerializer.Serialize(customer));
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var result = await httpClient.PostAsync(_config["ApiEndpoint"], content);
            var customerString = await result.Content.ReadAsStringAsync();
            Console.WriteLine($"Posted Customer: {customerString}");
            Console.ReadLine();
        }

        private static IConfigurationRoot GetConfig() => new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
    }
}
