using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerFakeRepository : ICustomerRepository
    {
        public void AddCustomer(Customer customer)
        {
            Console.WriteLine($"Saving customer with Id {customer.Id}. Name: {customer.FullName} on thread {Thread.CurrentThread.ManagedThreadId}. " +
                $"Thread is ThreadPool: {Thread.CurrentThread.IsThreadPoolThread}");
        }

        public Task AddCustomerAsync(Customer customer, CancellationToken token)
        {
            Console.WriteLine($"Saving customer async with Id {customer.Id}. Name: {customer.FullName} on thread {Thread.CurrentThread.ManagedThreadId}. " +
                $"Thread is ThreadPool: {Thread.CurrentThread.IsThreadPoolThread}");
            return Task.CompletedTask;
        }

        public Task<Customer> GetCustomerAsync(int id, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Customer>> SelectCustomersAsync(CancellationToken token)
        {
            throw new NotImplementedException();
        }
    }
}