﻿using Microsoft.Extensions.Caching.Memory;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepositoryWithCache : ICustomerRepository
    {
        private readonly ICustomerRepository _innerRepo;
        private readonly IMemoryCache _cache;
        private TimeSpan _slidingExpiration = TimeSpan.FromSeconds(30);
        private TimeSpan _absoluteExpiration = TimeSpan.FromMinutes(1);

        public CustomerRepositoryWithCache(ICustomerRepository innerRepo, IMemoryCache cache)
        {
            _innerRepo = innerRepo;
            _cache = cache;
        }
        public void AddCustomer(Customer customer)
        {
            _innerRepo.AddCustomer(customer);
            _cache.Set(customer.Id, customer, GetCustomersCahceOptions);
        }

        public async Task AddCustomerAsync(Customer customer, CancellationToken token)
        {
            await _innerRepo.AddCustomerAsync(customer, token);
            _cache.Set(customer.Id, customer, GetCustomersCahceOptions);
        }

        public Task<Customer> GetCustomerAsync(int id, CancellationToken token)
        {
            return _cache.GetOrCreateAsync(id, GetCacheFactoryWithDefaultOptions(
                async () => await _innerRepo.GetCustomerAsync(id, token)));
        }

        public Task<IEnumerable<Customer>> SelectCustomersAsync(CancellationToken token)
        {
            return _cache.GetOrCreateAsync($"_{nameof(SelectCustomersAsync)}", GetCacheFactoryWithDefaultOptions(
                async () => await _innerRepo.SelectCustomersAsync(token)));
        }

        private MemoryCacheEntryOptions GetCustomersCahceOptions =>
            new MemoryCacheEntryOptions()
            .SetSlidingExpiration(_slidingExpiration)
            .SetAbsoluteExpiration(_absoluteExpiration);

        private Func<ICacheEntry, Task<T>> GetCacheFactoryWithDefaultOptions<T>(Func<Task<T>> factory) => async entry =>
        {
            entry.SlidingExpiration = GetCustomersCahceOptions.SlidingExpiration;
            entry.AbsoluteExpirationRelativeToNow = GetCustomersCahceOptions.AbsoluteExpirationRelativeToNow;
            return await factory();
        };
    }
}
