﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.DataAccess.DbContexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private CustomerDbContext _context;

        public CustomerRepository(CustomerDbContext context)
        {
            _context = context;
        }

        public void AddCustomer(Customer customer)
        {
            AddCustomerInternal(customer);
            _context.SaveChanges();
        }

        public async Task AddCustomerAsync(Customer customer, CancellationToken token)
        {
            AddCustomerInternal(customer);
            await _context.SaveChangesAsync(token);
        }

        public async Task<Customer> GetCustomerAsync(int id, CancellationToken token)
        {
            return await _context.Customers.FirstOrDefaultAsync(c => c.Id == id, token);
        }

        public async Task<IEnumerable<Customer>> SelectCustomersAsync(CancellationToken token)
        {
            return await _context.Customers.Select(c => c).ToListAsync();
        }

        private void AddCustomerInternal(Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            var exists = _context.Customers.Find(customer.Id);
            if (exists == null)
                _context.Customers.Add(customer);
        }
    }
}
