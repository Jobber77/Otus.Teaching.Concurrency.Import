﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvDataParser : IDataParser
    {
        public IEnumerable<K> Parse<T, K>(string input) where T : IDataContainer<K>
            =>   CsvSerializer.DeserializeFromString<List<K>>(input);
    }
}
