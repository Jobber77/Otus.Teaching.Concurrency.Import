﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlDataParser : IDataParser
    {
        public IEnumerable<K> Parse<T, K>(string input) where T : IDataContainer<K>
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
            using (StringReader textReader = new StringReader(input))
            {
                var container = (T)xmlSerializer.Deserialize(textReader);
                return container.Data;
            }
        }
    }
}
