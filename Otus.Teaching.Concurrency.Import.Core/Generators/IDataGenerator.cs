using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Data
{
    public interface IDataGenerator
    {
        string Generate();

        Task<string> GenerateAsync(CancellationToken token);
    }
}