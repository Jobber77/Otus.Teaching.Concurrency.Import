using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);

        Task AddCustomerAsync(Customer customer, CancellationToken token);

        Task<IEnumerable<Customer>> SelectCustomersAsync(CancellationToken token);

        Task<Customer> GetCustomerAsync(int id, CancellationToken token);
    }
}