﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader<T>
    {
        void LoadData(IEnumerable<T> data);
        Task LoadDataAsync(IEnumerable<T> data);
    }
}