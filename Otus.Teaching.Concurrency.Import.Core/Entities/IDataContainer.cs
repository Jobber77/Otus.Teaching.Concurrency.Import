﻿using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    public interface IDataContainer<T>
    {
        public List<T> Data { get; set; }
    }
}
