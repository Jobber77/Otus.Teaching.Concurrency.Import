﻿using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public string Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            var filename = $"{_fileName}.csv";
            using var stream = File.Create(filename);
            using var streamWrighter = new StreamWriter(stream);
            streamWrighter.WriteCsv(customers);

            return filename;
        }

        public async Task<string> GenerateAsync(CancellationToken token)
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            var filename = $"{_fileName}.csv";
            var customersCsv = CsvSerializer.SerializeToCsv(customers);
            using var sw = new StreamWriter(filename);
            await sw.WriteAsync(customersCsv);

            return filename;
        }
    }
}
