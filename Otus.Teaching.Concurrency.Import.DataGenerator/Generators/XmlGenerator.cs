using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class XmlGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public XmlGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }
        
        public string Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            var filename = $"{_fileName}.xml";
            using var stream = File.Create(filename);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Data = customers
            });
            return filename;
        }

        public async Task<string> GenerateAsync(CancellationToken token)
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            var filename = $"{_fileName}.xml";
            using var memoryStream = new MemoryStream();
            new XmlSerializer(typeof(CustomersList)).Serialize(memoryStream, new CustomersList()
            {
                Data = customers
            });
            using var fileStream = File.Create(filename);
            await fileStream.WriteAsync(memoryStream.ToArray(), 0, (int)memoryStream.Length);
            return filename;
        }
    }
}